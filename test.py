import torch
import numpy as np
from opticexp import PhysDevice

phys_device = PhysDevice()

phi_0 = torch.tensor([0.0, 1.0, 0.0], dtype=torch.complex128, device="cpu")
alpha = torch.tensor(np.random.randn(3, 3), dtype=torch.complex128, device="cpu")
phases_block_left = torch.tensor(np.random.randn(9), dtype=torch.complex128, device="cpu")
phases_block_right = torch.tensor(np.random.randn(9), dtype=torch.complex128, device="cpu")

currents = torch.tensor(np.random.randn(3), dtype=torch.complex128, device="cpu")

phys_device.set_fix_params(phi_0, alpha, phases_block_left, phases_block_right)

# print(phi_0)
# print(alpha)
# print(phases_block_left)
# print(phases_block_right)

# print(torch.eye(4, dtype=torch.complex128, device="cpu"))

torch.set_printoptions(precision=2, sci_mode=False)

u = phys_device.get_u_device(currents)
print(u @ torch.conj(u.T))
print(u)

print(phys_device.get_u_device_real_bond(currents))

u_real_bond = phys_device.get_u_device_real_bond(currents)

print(u_real_bond @ torch.conj(u_real_bond.T))
