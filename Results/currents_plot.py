import csv
import numpy as np
import matplotlib.pyplot as plt

# Пути к файлу, который вы хотите прочитать
file_path = 'currents.csv'

# Инициализируем пустые списки для хранения данных x и y
fidelity_list = []
fid_fit_list = []

# Открываем файл для чтения
with open(file_path, 'r') as file:
    reader = csv.reader(file)
    next(reader)  # Пропускаем заголовок

    # Читаем данные из csv
    for row in reader:
        fidelity_list.append(float(row[0]))  # Добавляем значение x, преобразуем к типу int (или float, если нужно)
        fid_fit_list.append(float(row[1]))  # Добавляем значение y, также преобразуем

# # Создаем график
# plt.figure(figsize=(8, 6))  # Устанавливаем размер фигуры
#
# # Добавляем точки для диаграммы рассеяния
# plt.scatter(x, y, color='blue', label='Данные (x, y)')
#
# # Добавляем пунктирную линию y = x
# plt.plot(x, x, '--', color='red', label='Линия y = x')
#
# # Настраиваем подписи
# plt.xlabel('Ось X')
# plt.ylabel('Ось Y')
# plt.title('Диаграмма рассеяния с линией y = x')
# plt.legend()  # Добавляем легенду
#
# # Отображаем график
# plt.grid(True)  # Включаем сетку
# plt.show()

print(len(fidelity_list))
print(np.mean(np.array(fidelity_list)), np.std(np.array(fidelity_list)))

fig, ax = plt.subplots(figsize=(8, 6))

# Рассчитываем гистограмму
counts, bins = np.histogram(fidelity_list, bins=100)

# Нормализуем гистограмму, чтобы получить вероятности
counts_normalized = counts / counts.sum()

# Строим нормализованную гистограмму с чёрной обводкой каждого бина
plt.bar(bins[:-1], counts_normalized, width=np.diff(bins), color='navy', alpha=0.5, edgecolor='black', align='edge')

# Добавляем сетку со сплошной линией
plt.grid(True, linestyle='-', linewidth=0.5, color='gray', alpha=0.5)

plt.title('36 test matrices, mean +/- std: 0.971 +/- 0.022', fontsize=18)

plt.tick_params(which='major', labelsize=18)
plt.tick_params(which='minor', labelsize=18)

# ax.legend(loc='upper right')
plt.xlabel('Fidelity with predicted by model', fontsize=20)
plt.ylabel('Probability', fontsize=20)
plt.show()
