# 970_U_520_254_133.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  520.00  254.00  133.00 
# Corresponding phaseshifts (rad): 
   0.00    2.64    1.47   -0.24 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.3086   0.1501   0.5353   0.7718 
  0.1263  -0.2747  -0.5899   0.3760 
 -0.2638   0.4568  -0.4057   0.2811 
 -0.5048  -0.2875  -0.0127   0.4290 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.1579   0.5743  -0.2539   0.0000 
  0.4861  -0.4474  -0.2042   0.0000 
 -0.5504  -0.2841   0.3077   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-10-02 16:35:53
#
# Fits R_sq for phase retrieve (mean = 0.989):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9947   0.9493   0.9878   0.9963 
#  In_42   0.9707   0.9983   0.9945   0.9977 
#  In_43   0.9958   0.9998   0.9955   0.9837 
#
# Fidelity with closest unitary = 0.993
#
# Fidelity between closest unitary and U predicted by model = 0.97
# U predicted by model: 
# REAL
  0.3595   0.1885   0.5164   0.7541 
  0.1909  -0.3681  -0.5978   0.4104 
 -0.3464   0.5056  -0.4055   0.3164 
 -0.5942  -0.3743  -0.0390   0.4036 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.0568   0.4998  -0.2220   0.0000 
  0.4415  -0.3672  -0.1733   0.0000 
 -0.4039  -0.2203   0.3616   0.0000 
