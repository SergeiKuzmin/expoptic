# 941_U_818_388_540.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  818.00  388.00  540.00 
# Corresponding phaseshifts (rad): 
   0.00    5.62    3.07    1.43 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.6066   0.6844   0.1036   0.3911 
 -0.5724   0.3675   0.4353   0.2472 
  0.0289  -0.5316  -0.0405   0.8294 
 -0.3859   0.2569  -0.5549   0.3125 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.3698  -0.1789   0.3435   0.0000 
 -0.1340   0.0306   0.0904   0.0000 
  0.0106   0.1219  -0.6038   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-10-02 14:06:33
#
# Fits R_sq for phase retrieve (mean = 0.962):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9958   0.9661   0.9897   0.8888 
#  In_42   0.9993   0.9951   0.9986   0.9569 
#  In_43   0.7756   0.9930   0.9838   0.9995 
#
# Fidelity with closest unitary = 0.992
#
# Fidelity between closest unitary and U predicted by model = 0.941
# U predicted by model: 
# REAL
  0.6042   0.6763   0.1186   0.4043 
 -0.6698   0.3883   0.2925   0.2655 
  0.0456  -0.5366  -0.0221   0.8360 
 -0.4035   0.2782  -0.4136   0.2590 
# IMAG
  0.0000   0.0000  -0.0000   0.0000 
  0.0824  -0.1547   0.4625   0.0000 
 -0.0589   0.0396   0.0743   0.0000 
  0.1057   0.0308  -0.7137   0.0000 
