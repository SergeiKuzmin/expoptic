# 989_U_585_603_321.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  585.00  603.00  321.00 
# Corresponding phaseshifts (rad): 
   0.00    3.92    4.28    1.00 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.3564   0.2619   0.2119   0.8715 
 -0.2417  -0.2408  -0.8423   0.4092 
 -0.2009  -0.8228   0.4331   0.2463 
 -0.8547   0.3402   0.2174   0.1115 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.0359   0.0454   0.0569   0.0000 
 -0.0986   0.1475  -0.0544   0.0000 
  0.1782  -0.2388  -0.0728   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-26 19:17:05
#
# Fits R_sq for phase retrieve (mean = 0.961):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9972   0.9527   0.8769   0.9902 
#  In_42   0.9985   0.9167   0.9787   0.9942 
#  In_43   0.9842   0.9984   0.9980   0.8414 
#
# Fidelity with closest unitary = 0.998
#
# Fidelity between closest unitary and U predicted by model = 0.989
# U predicted by model: 
# REAL
  0.3699   0.2696   0.1935   0.8678 
 -0.2770  -0.2172  -0.8501   0.3751 
 -0.2021  -0.8428   0.4102   0.2565 
 -0.8218   0.3170   0.2275   0.2011 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.0740   0.0792   0.0310   0.0000 
 -0.0810   0.0725   0.0539   0.0000 
  0.2413  -0.2403  -0.1266   0.0000 
