# 994_U_948_607_593.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  948.00  607.00  593.00 
# Corresponding phaseshifts (rad): 
   0.00    7.62    5.03    1.82 
# Experimentally obtained matrix (may not be unitary): 
# REAL
 -0.1246  -0.6140   0.7449  -0.0483 
 -0.2640   0.4369   0.3963  -0.4242 
  0.4251   0.3452   0.2024  -0.4998 
  0.1569   0.4479   0.4980   0.7258 
# IMAG
  0.1651  -0.1414   0.0000   0.0547 
 -0.5591   0.2989   0.0000  -0.0240 
  0.6072  -0.0582   0.0000  -0.1929 
  0.0000   0.0000   0.0000   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-10-19 13:40:35
#
# Fits R_sq for phase retrieve (mean = 0.981):
#          Out_1    Out_2    Out_3    Out_4  
#  In_31   0.9846   0.9980   0.9932   0.9940 
#  In_32   0.9912   0.9959   0.8920   0.9892 
#  In_34   0.9918   0.9969   0.9478   0.9954 
#
# Fidelity with closest unitary = 0.998
#
# Fidelity between closest unitary and U predicted by model = 0.994
# U predicted by model: 
# REAL
  0.1098   0.6173   0.7723   0.1024 
  0.2342  -0.4941   0.3041   0.4343 
 -0.4773  -0.1691   0.1353   0.5106 
  0.1780   0.3234  -0.3813   0.7350 
# IMAG
  0.0000  -0.0000  -0.0000   0.0000 
  0.5681   0.1990  -0.2399   0.0000 
 -0.5878   0.3053  -0.1605   0.0000 
  0.0727  -0.3298   0.2533   0.0000 
