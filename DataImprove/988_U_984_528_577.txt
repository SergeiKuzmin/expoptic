# 988_U_984_528_577.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  984.00  528.00  577.00 
# Corresponding phaseshifts (rad): 
   0.00    7.79    4.18    1.26 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.3227   0.5909   0.6483   0.3555 
  0.5848  -0.3768  -0.2183   0.5124 
 -0.5766   0.3624  -0.3811   0.4935 
 -0.2112  -0.3482   0.1063   0.6063 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.0275   0.1941  -0.4090   0.0000 
  0.3064   0.2172  -0.0799   0.0000 
 -0.2871  -0.4139   0.4489   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 16:46:43
#
# Fits R_sq for phase retrieve (mean = 0.993):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9937   0.9927   0.9917   0.9962 
#  In_42   0.9916   0.9863   0.9967   0.9921 
#  In_43   0.9998   0.9955   0.9918   0.9924 
#
# Fidelity with closest unitary = 0.99
#
# Fidelity between closest unitary and U predicted by model = 0.988
# U predicted by model: 
# REAL
  0.2788   0.5949   0.6596   0.3651 
  0.5025  -0.2849  -0.2380   0.5105 
 -0.6241   0.4390  -0.3931   0.4716 
 -0.1033  -0.4501   0.1067   0.6194 
# IMAG
 -0.0000  -0.0000   0.0000   0.0000 
  0.3070   0.3034  -0.4034   0.0000 
  0.1701   0.0347  -0.1032   0.0000 
 -0.3826  -0.2765   0.4110   0.0000 
