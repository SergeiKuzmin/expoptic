# 985_U_433_769_232.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  433.00  769.00  232.00 
# Corresponding phaseshifts (rad): 
   0.00    3.48    6.07    1.45 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.5940   0.1825   0.6592   0.4235 
  0.1316   0.6917  -0.4741   0.4925 
 -0.2408  -0.3143   0.0558   0.3178 
 -0.2666  -0.4849  -0.1049   0.6906 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.1616   0.0152   0.1022   0.0000 
 -0.6313   0.3647   0.4557   0.0000 
  0.2765  -0.1444  -0.3296   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-10-02 11:37:12
#
# Fits R_sq for phase retrieve (mean = 0.971):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9819   0.9741   0.9396   0.9811 
#  In_42   0.8688   0.9802   0.9806   0.9977 
#  In_43   0.9877   0.9989   0.9635   0.9965 
#
# Fidelity with closest unitary = 0.995
#
# Fidelity between closest unitary and U predicted by model = 0.985
# U predicted by model: 
# REAL
  0.6102   0.1769   0.6628   0.3964 
 -0.0243   0.6617  -0.4393   0.4767 
 -0.4724  -0.1530   0.2801   0.3271 
 -0.1062  -0.4704  -0.2032   0.7132 
# IMAG
 -0.0000   0.0000   0.0000   0.0000 
 -0.1877  -0.2272   0.2335   0.0000 
 -0.4859   0.4795   0.3194   0.0000 
  0.3483  -0.0681  -0.3025   0.0000 
