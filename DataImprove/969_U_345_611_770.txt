# 969_U_345_611_770.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  345.00  611.00  770.00 
# Corresponding phaseshifts (rad): 
   0.00    3.28    6.32    6.28 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.6667   0.3934   0.4867   0.4049 
 -0.4252  -0.3341   0.4287   0.3941 
 -0.4835   0.3057   0.0766   0.6024 
  0.2764  -0.3033  -0.6274   0.5642 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.0045  -0.4868   0.3626   0.0000 
 -0.1883   0.5068  -0.1087   0.0000 
  0.1710  -0.2327  -0.1904   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-26 17:40:22
#
# Fits R_sq for phase retrieve (mean = 0.98):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9954   0.9491   0.9967   0.9716 
#  In_42   0.9612   0.9929   0.9994   0.9959 
#  In_43   0.9706   0.9879   0.9418   0.9998 
#
# Fidelity with closest unitary = 0.997
#
# Fidelity between closest unitary and U predicted by model = 0.969
# U predicted by model: 
# REAL
  0.6779   0.3881   0.4950   0.3804 
 -0.3710  -0.4571   0.5650   0.3923 
 -0.4833   0.3303  -0.0555   0.5966 
  0.2995  -0.2813  -0.6411   0.5878 
# IMAG
  0.0000  -0.0000   0.0000   0.0000 
  0.1776  -0.3817   0.0560   0.0000 
 -0.2015   0.4951  -0.1123   0.0000 
  0.0859  -0.2478   0.0766   0.0000 
