import torch

x = torch.tensor([1.0, 2.0, 3.0, 4.0], dtype=torch.float64, device='cpu')

print(x)

x.requires_grad_(True)

print(x)

f = 3 * x[0] + 2 * x[1]

print(f)

f.backward()

print(x.grad)
