# 990_U_119_261_160.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  119.00  261.00  160.00 
# Corresponding phaseshifts (rad): 
   0.00    1.12    1.63    0.56 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.5906   0.1628   0.5928   0.5227 
  0.1006  -0.7586  -0.1997   0.5550 
 -0.3909   0.2429  -0.3422   0.4546 
 -0.1889   0.4845  -0.1872   0.4604 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.0717  -0.1940  -0.1539   0.0000 
 -0.4518  -0.0217   0.5097   0.0000 
  0.4938   0.2553  -0.4166   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 13:58:31
#
# Fits R_sq for phase retrieve (mean = 0.978):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9624   0.9299   0.9478   0.9895 
#  In_42   0.9843   0.9997   0.9554   0.9884 
#  In_43   0.9962   0.9980   0.9974   0.9841 
#
# Fidelity with closest unitary = 0.99
#
# Fidelity between closest unitary and U predicted by model = 0.99
# U predicted by model: 
# REAL
  0.6126   0.1623   0.5673   0.5259 
 -0.0525  -0.7773  -0.2374   0.5571 
 -0.2862   0.2033  -0.1491   0.4315 
 -0.3557   0.5457  -0.2137   0.4763 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.1052   0.0258  -0.1210   0.0000 
 -0.5266  -0.1417   0.6091   0.0000 
  0.3539   0.0981  -0.4102   0.0000 
