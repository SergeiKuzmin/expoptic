# 978_U_307_153_324.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  307.00  153.00  324.00 
# Corresponding phaseshifts (rad): 
   0.00    1.60    1.53    0.99 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.4127   0.0939   0.7218   0.5476 
  0.0512  -0.8503  -0.1806   0.3794 
 -0.3678   0.1807  -0.2983   0.3868 
 -0.0131   0.3254  -0.3277   0.6376 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.0057  -0.3098  -0.0429   0.0000 
 -0.6526   0.0179   0.4089   0.0000 
  0.5158   0.1812  -0.2848   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 16:15:10
#
# Fits R_sq for phase retrieve (mean = 0.935):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9259   0.4848   0.9648   0.9895 
#  In_42   0.9571   0.9994   0.9287   0.9880 
#  In_43   0.9998   0.9993   0.9948   0.9933 
#
# Fidelity with closest unitary = 0.993
#
# Fidelity between closest unitary and U predicted by model = 0.978
# U predicted by model: 
# REAL
  0.4432   0.0788   0.7070   0.5454 
 -0.0109  -0.8921  -0.1888   0.3826 
 -0.1600   0.1455  -0.2153   0.3881 
 -0.2755   0.3798  -0.3609   0.6368 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.0374   0.1380  -0.0388   0.0000 
 -0.7245  -0.1152   0.4670   0.0000 
  0.4191  -0.0127  -0.2613   0.0000 
