# 989_U_444_739_189.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  444.00  739.00  189.00 
# Corresponding phaseshifts (rad): 
   0.00    3.40    5.61    1.16 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.5532   0.2392   0.5866   0.5410 
  0.0453   0.5110  -0.5991   0.4799 
 -0.1508  -0.5760   0.0390   0.3085 
 -0.4198  -0.3693  -0.0936   0.6178 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.2324   0.2899   0.0976   0.0000 
 -0.5873   0.1647   0.4204   0.0000 
  0.3065  -0.3203  -0.3168   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-29 16:14:56
#
# Fits R_sq for phase retrieve (mean = 0.981):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9945   0.9747   0.9504   0.9838 
#  In_42   0.9622   0.9634   0.9919   0.9967 
#  In_43   0.9912   0.9985   0.9641   0.9953 
#
# Fidelity with closest unitary = 0.995
#
# Fidelity between closest unitary and U predicted by model = 0.989
# U predicted by model: 
# REAL
  0.5695   0.2383   0.5878   0.5229 
 -0.0594   0.5818  -0.5847   0.4567 
 -0.3003  -0.4647   0.2024   0.3114 
 -0.2730  -0.3786  -0.1592   0.6489 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.2440   0.0803   0.2039   0.0000 
 -0.5200   0.4164   0.3350   0.0000 
  0.4213  -0.2563  -0.3043   0.0000 
