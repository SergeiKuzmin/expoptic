# 967_U_360_262_583.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  360.00  262.00  583.00 
# Corresponding phaseshifts (rad): 
   0.00    2.25    2.79    3.21 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.2311   0.4874   0.5524   0.6355 
 -0.2427  -0.7064   0.3657   0.1923 
 -0.3118  -0.3872  -0.2147   0.6542 
  0.2053   0.0558  -0.6735   0.3622 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.4377   0.2330  -0.1598   0.0000 
  0.4838  -0.1526   0.1464   0.0000 
 -0.5681   0.1811  -0.1201   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 19:34:56
#
# Fits R_sq for phase retrieve (mean = 0.971):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9032   0.8758   0.9983   0.9874 
#  In_42   0.9813   0.9663   0.9988   0.9750 
#  In_43   0.9998   0.9941   0.9771   0.9970 
#
# Fidelity with closest unitary = 0.989
#
# Fidelity between closest unitary and U predicted by model = 0.967
# U predicted by model: 
# REAL
  0.1925   0.5074   0.5699   0.6170 
  0.0483  -0.6164   0.3335   0.1838 
 -0.5477  -0.3397  -0.2176   0.6513 
  0.5704   0.0535  -0.6752   0.4017 
# IMAG
 -0.0000  -0.0000   0.0000   0.0000 
 -0.5131   0.4139  -0.1953   0.0000 
  0.2300  -0.2165   0.1151   0.0000 
 -0.1381   0.1616  -0.0972   0.0000 
