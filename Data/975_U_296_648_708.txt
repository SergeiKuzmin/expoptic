# 975_U_296_648_708.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  296.00  648.00  708.00 
# Corresponding phaseshifts (rad): 
   0.00    3.08    6.37    5.60 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.6601   0.4462   0.4622   0.3893 
 -0.2928  -0.4225   0.4718   0.5223 
 -0.4630   0.3856  -0.2384   0.6057 
  0.3044  -0.3852  -0.4885   0.4569 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.3620  -0.2085  -0.2565   0.0000 
 -0.1603   0.4011  -0.1633   0.0000 
 -0.1212  -0.3478   0.4194   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-10-02 12:41:13
#
# Fits R_sq for phase retrieve (mean = 0.986):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9918   0.9765   0.9933   0.9538 
#  In_42   0.9843   0.9974   0.9945   0.9984 
#  In_43   0.9621   0.9965   0.9892   0.9996 
#
# Fidelity with closest unitary = 0.997
#
# Fidelity between closest unitary and U predicted by model = 0.975
# U predicted by model: 
# REAL
  0.6641   0.4528   0.4616   0.3754 
 -0.1900  -0.4270   0.2618   0.5293 
 -0.4247   0.4105  -0.2877   0.6100 
  0.2426  -0.4274  -0.2997   0.4548 
# IMAG
  0.0000  -0.0000   0.0000   0.0000 
  0.4370  -0.1707  -0.4613   0.0000 
 -0.2349   0.3745  -0.0294   0.0000 
 -0.1936  -0.3036   0.5763   0.0000 
