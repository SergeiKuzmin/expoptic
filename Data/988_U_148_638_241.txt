# 988_U_148_638_241.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  148.00  638.00  241.00 
# Corresponding phaseshifts (rad): 
   0.00    2.04    4.60    1.56 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.3072   0.5294   0.6955   0.3764 
  0.3089  -0.2565  -0.3450   0.5738 
 -0.2201   0.3693  -0.3963   0.4026 
 -0.3609  -0.3106   0.0904   0.6059 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.4186   0.3930  -0.2466   0.0000 
  0.6747   0.0896  -0.1811   0.0000 
 -0.0372  -0.5085   0.3715   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 18:52:53
#
# Fits R_sq for phase retrieve (mean = 0.991):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9724   0.9964   0.9941   0.9988 
#  In_42   0.9918   0.9801   0.9987   0.9957 
#  In_43   0.9967   0.9888   0.9944   0.9852 
#
# Fidelity with closest unitary = 0.995
#
# Fidelity between closest unitary and U predicted by model = 0.988
# U predicted by model: 
# REAL
  0.2502   0.5275   0.7097   0.3943 
  0.4318  -0.1260  -0.3704   0.5612 
 -0.2619   0.4032  -0.4134   0.3708 
 -0.3896  -0.4581   0.1300   0.6261 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.3041   0.4490  -0.2265   0.0000 
  0.6521  -0.0828  -0.1683   0.0000 
 -0.1136  -0.3535   0.3028   0.0000 
