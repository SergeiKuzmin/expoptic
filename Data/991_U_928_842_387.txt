# 991_U_928_842_387.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  928.00  842.00  387.00 
# Corresponding phaseshifts (rad): 
   0.00    7.95    7.14    0.73 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.3003   0.1768   0.8082   0.4746 
  0.0513  -0.2152  -0.1217   0.2622 
  0.3085   0.0579  -0.4253   0.1767 
 -0.2640  -0.0377  -0.2495   0.8213 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.1827   0.9018  -0.1444   0.0000 
 -0.7705  -0.1772   0.2536   0.0000 
  0.3392  -0.2714  -0.0580   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-10-02 17:18:33
#
# Fits R_sq for phase retrieve (mean = 0.974):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9017   0.9899   0.9491   0.9935 
#  In_42   0.9551   0.9974   0.9358   0.9924 
#  In_43   0.9989   0.9949   0.9956   0.9897 
#
# Fidelity with closest unitary = 0.993
#
# Fidelity between closest unitary and U predicted by model = 0.991
# U predicted by model: 
# REAL
  0.3681   0.2122   0.7825   0.4552 
 -0.0292  -0.0742  -0.1298   0.2814 
  0.3729   0.0182  -0.3080   0.2194 
 -0.2956  -0.0977  -0.3091   0.8158 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.2087   0.9120  -0.1492   0.0000 
 -0.7230  -0.1991   0.3941   0.0000 
  0.2664  -0.2610  -0.0545   0.0000 
