# 988_U_830_155_622.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  830.00  155.00  622.00 
# Corresponding phaseshifts (rad): 
   0.00    5.56    2.36    2.01 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.6956   0.6825   0.1498   0.1672 
 -0.5168   0.3245   0.2653   0.2051 
 -0.0327  -0.2560   0.2598   0.8979 
  0.0862   0.1861  -0.8120   0.3531 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.4500   0.5553   0.0652   0.0000 
  0.1346  -0.1377   0.1505   0.0000 
 -0.1379   0.0257  -0.3928   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 14:09:02
#
# Fits R_sq for phase retrieve (mean = 0.96):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9932   0.9891   0.9478   0.7415 
#  In_42   0.9906   0.9975   0.9955   0.9804 
#  In_43   0.9255   0.9661   0.9982   0.9987 
#
# Fidelity with closest unitary = 0.993
#
# Fidelity between closest unitary and U predicted by model = 0.988
# U predicted by model: 
# REAL
  0.7093   0.6726   0.1210   0.1728 
 -0.4285   0.3522   0.2807   0.1916 
 -0.0275  -0.2486   0.2500   0.9056 
 -0.0461   0.1229  -0.8943   0.3367 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.5139   0.5547  -0.0704   0.0000 
  0.1615  -0.1602  -0.0559   0.0000 
 -0.1418   0.1153   0.1905   0.0000 
