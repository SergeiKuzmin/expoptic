# 959_U_646_758_302.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  646.00  758.00  302.00 
# Corresponding phaseshifts (rad): 
   0.00    4.87    5.97    1.13 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.7670   0.1013   0.2677   0.5742 
  0.0122  -0.3414  -0.6181   0.4569 
 -0.2669   0.0157  -0.2729   0.5840 
 -0.5677   0.2782   0.5889   0.3473 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.0275   0.4572  -0.2877   0.0000 
  0.0202  -0.6885   0.1963   0.0000 
 -0.1314   0.3352   0.0567   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 18:00:19
#
# Fits R_sq for phase retrieve (mean = 0.94):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9893   0.6998   0.9767   0.9859 
#  In_42   0.6781   0.9787   0.9968   0.9947 
#  In_43   0.9818   0.9981   0.9960   0.9992 
#
# Fidelity with closest unitary = 0.991
#
# Fidelity between closest unitary and U predicted by model = 0.959
# U predicted by model: 
# REAL
  0.7870   0.0746   0.2802   0.5446 
 -0.0598   0.0497  -0.6421   0.4100 
 -0.2909  -0.2718  -0.2540   0.5883 
 -0.5354   0.2273   0.5978   0.4350 
# IMAG
  0.0000   0.0000  -0.0000   0.0000 
  0.0361   0.5881  -0.2578   0.0000 
  0.0217  -0.6465   0.1112   0.0000 
 -0.0633   0.3202   0.0926   0.0000 
