# 990_U_0_0_0.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00    0.00    0.00    0.00 
# Corresponding phaseshifts (rad): 
   0.00    0.83    0.97    0.21 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.6500   0.1699   0.5963   0.4394 
 -0.1015  -0.4091  -0.0735   0.5723 
 -0.3923   0.1433  -0.2208   0.3640 
  0.0395   0.1958  -0.2317   0.5891 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.0619   0.6670  -0.2016   0.0000 
 -0.4674  -0.1716   0.6296   0.0000 
  0.4349  -0.5206  -0.3149   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-26 19:36:25
#
# Fits R_sq for phase retrieve (mean = 0.969):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9442   0.9090   0.9205   0.9951 
#  In_42   0.9868   0.9990   0.8987   0.9942 
#  In_43   0.9993   0.9960   0.9985   0.9902 
#
# Fidelity with closest unitary = 0.988
#
# Fidelity between closest unitary and U predicted by model = 0.99
# U predicted by model: 
# REAL
  0.6535   0.2022   0.5832   0.4381 
 -0.1605  -0.3028  -0.1426   0.5690 
 -0.2742   0.0521   0.0386   0.3336 
 -0.1695   0.1086  -0.3065   0.6107 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.0776   0.7119  -0.1599   0.0000 
 -0.5471  -0.2046   0.6841   0.0000 
  0.3712  -0.5515  -0.2248   0.0000 
