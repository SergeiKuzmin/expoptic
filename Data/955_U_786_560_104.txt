# 955_U_786_560_104.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  786.00  560.00  104.00 
# Corresponding phaseshifts (rad): 
   0.00    5.32    3.39   -0.74 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.5420   0.5903   0.3835   0.4590 
 -0.2646  -0.1317  -0.2288   0.3474 
  0.0205  -0.2482   0.0923   0.2500 
 -0.3294  -0.2002  -0.1458   0.7791 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.4818   0.1225   0.7017   0.0000 
 -0.4024   0.6667  -0.5105   0.0000 
  0.3650  -0.2699  -0.1311   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 12:55:27
#
# Fits R_sq for phase retrieve (mean = 0.963):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9867   0.9293   0.8685   0.9951 
#  In_42   0.9995   0.8502   0.9638   0.9977 
#  In_43   0.9932   0.9981   0.9907   0.9814 
#
# Fidelity with closest unitary = 0.992
#
# Fidelity between closest unitary and U predicted by model = 0.955
# U predicted by model: 
# REAL
  0.5467   0.5806   0.3995   0.4520 
  0.1052  -0.1040  -0.4008   0.3605 
 -0.0994  -0.1644   0.1193   0.2259 
 -0.3350  -0.2396  -0.0804   0.7840 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.5643   0.1219   0.5952   0.0000 
 -0.3468   0.6976  -0.5393   0.0000 
  0.3594  -0.2570  -0.1183   0.0000 
