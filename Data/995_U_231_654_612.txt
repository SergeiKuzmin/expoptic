# 995_U_231_654_612.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  231.00  654.00  612.00 
# Corresponding phaseshifts (rad): 
   0.00    2.72    5.97    4.51 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.5696   0.5881   0.4999   0.2826 
 -0.2244  -0.1492  -0.0931   0.6475 
 -0.1907   0.2410  -0.2987   0.6135 
  0.1299  -0.6014   0.5555   0.3539 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
  0.5394  -0.2378  -0.3899   0.0000 
 -0.5003   0.3068   0.3091   0.0000 
 -0.1771  -0.2461   0.3093   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-26 17:11:21
#
# Fits R_sq for phase retrieve (mean = 0.975):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9258   0.9923   0.9783   0.8762 
#  In_42   0.9958   0.9978   0.9943   0.9955 
#  In_43   0.9639   0.9827   0.9969   0.9993 
#
# Fidelity with closest unitary = 0.994
#
# Fidelity between closest unitary and U predicted by model = 0.995
# U predicted by model: 
# REAL
  0.5802   0.5698   0.5166   0.2679 
 -0.1562  -0.0862  -0.0740   0.6643 
 -0.1143   0.1838  -0.3951   0.6187 
  0.0590  -0.6479   0.4809   0.3227 
# IMAG
  0.0000  -0.0000   0.0000   0.0000 
  0.5699  -0.2458  -0.3689   0.0000 
 -0.5071   0.3531   0.1800   0.0000 
 -0.2011  -0.1709   0.4144   0.0000 
