# 989_U_285_657_880.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  285.00  657.00  880.00 
# Corresponding phaseshifts (rad): 
   0.00    3.45    7.51    8.23 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.4470   0.2246   0.7855   0.3642 
 -0.0229  -0.7870   0.1310   0.2398 
 -0.3018  -0.1794   0.2307   0.3340 
 -0.0610   0.2250  -0.3616   0.8358 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.2686   0.4765   0.0793   0.0000 
  0.7464  -0.0668  -0.3878   0.0000 
 -0.2753  -0.1238   0.1591   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-30 14:30:03
#
# Fits R_sq for phase retrieve (mean = 0.981):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9837   0.9637   0.9871   0.9666 
#  In_42   0.9676   0.9974   0.9664   0.9991 
#  In_43   0.9771   0.9861   0.9792   0.9992 
#
# Fidelity with closest unitary = 0.996
#
# Fidelity between closest unitary and U predicted by model = 0.989
# U predicted by model: 
# REAL
  0.4824   0.2209   0.7732   0.3474 
 -0.0100  -0.8911   0.1496   0.2475 
 -0.3254  -0.1451   0.0785   0.3695 
 -0.0544   0.2391  -0.4054   0.8255 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.2343   0.2483   0.0753   0.0000 
  0.7328  -0.1305  -0.4199   0.0000 
 -0.2577  -0.0160   0.1654   0.0000 
