# 996_U_291_382_797.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  291.00  382.00  797.00 
# Corresponding phaseshifts (rad): 
   0.00    2.58    4.58    6.29 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.3094   0.5214   0.4283   0.6700 
  0.1907   0.2561  -0.7163   0.2475 
  0.2858  -0.7536   0.1867   0.3906 
 -0.5594  -0.2656  -0.3087   0.5805 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.4952   0.1253   0.2505   0.0000 
 -0.3314   0.0193   0.2296   0.0000 
  0.3442  -0.0867  -0.2414   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-09-26 17:21:02
#
# Fits R_sq for phase retrieve (mean = 0.973):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9926   0.9240   0.9926   0.9801 
#  In_42   0.9935   0.8244   0.9980   0.9976 
#  In_43   0.9889   0.9926   0.9981   0.9936 
#
# Fidelity with closest unitary = 0.998
#
# Fidelity between closest unitary and U predicted by model = 0.996
# U predicted by model: 
# REAL
  0.3006   0.5243   0.4481   0.6588 
  0.1025   0.2594  -0.7331   0.2454 
  0.1884  -0.7556   0.2354   0.3552 
 -0.4709  -0.2283  -0.3229   0.6161 
# IMAG
  0.0000   0.0000  -0.0000   0.0000 
 -0.5184   0.1274   0.1987   0.0000 
 -0.4166   0.0899   0.1743   0.0000 
  0.4467  -0.1026  -0.1796   0.0000 
