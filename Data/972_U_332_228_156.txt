# 972_U_332_228_156.txt
# Currents on target chip heaters:
#  Heat_8  Heat_9 Heat_10 Heat_11
   0.00  332.00  228.00  156.00 
# Corresponding phaseshifts (rad): 
   0.00    1.65    1.45    0.24 
# Experimentally obtained matrix (may not be unitary): 
# REAL
  0.3744   0.1550   0.6163   0.6752 
  0.0059  -0.5367  -0.3775   0.4824 
  0.0623   0.1242  -0.5863   0.1444 
 -0.4728   0.3072  -0.1975   0.5390 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.1385   0.5510  -0.1182   0.0000 
 -0.6349  -0.3701   0.2757   0.0000 
  0.4580  -0.3717  -0.0680   0.0000 
# 
# This data were measured: 
# Module measuremnt started 2023-10-02 13:55:53
#
# Fits R_sq for phase retrieve (mean = 0.966):
#          Out_1    Out_2    Out_3    Out_4  
#  In_41   0.9475   0.8399   0.8871   0.9954 
#  In_42   0.9770   0.9991   0.9781   0.9974 
#  In_43   0.9989   0.9982   0.9961   0.9777 
#
# Fidelity with closest unitary = 0.985
#
# Fidelity between closest unitary and U predicted by model = 0.972
# U predicted by model: 
# REAL
  0.4321   0.1476   0.6053   0.6520 
 -0.0717  -0.4104  -0.3809   0.4941 
  0.2856  -0.0025  -0.3980   0.1808 
 -0.5457   0.1959  -0.2463   0.5460 
# IMAG
  0.0000   0.0000   0.0000   0.0000 
 -0.1045   0.6476  -0.0833   0.0000 
 -0.5795  -0.3707   0.5040   0.0000 
  0.2865  -0.4633  -0.0915   0.0000 
