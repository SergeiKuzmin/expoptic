import torch
import numpy as np


class Data(object):
    def __init__(self):
        self.train_data = None
        self.test_data = None
        self.M = None
        self.K = None

    def set_train_data(self, train_data):
        self.train_data = train_data
        self.M = len(train_data)

    def set_test_data(self, test_data):
        self.test_data = test_data
        self.K = len(test_data)

    def get_mini_batch_train(self, mini_batch_size):
        x = np.random.randint(0, len(self.train_data), mini_batch_size)
        mini_batch_train = [self.train_data[x[i]] for i in range(mini_batch_size)]
        return mini_batch_train

    def get_test_data(self):
        return self.test_data
