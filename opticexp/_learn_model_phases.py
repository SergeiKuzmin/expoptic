import torch
from torch import optim
import numpy as np
from opticexp import Data


class LearnModelPhases(object):
    def __init__(self, data: Data):
        self.block_left = None
        self.block_right = None
        self.data = data

    def set_start_parameters(self, block_left=None, block_right=None):
        if block_left is None:
            block_left = torch.tensor(LearnModelPhases.generator_unitary_matrix(), dtype=torch.complex128, device="cpu")
        if block_right is None:
            block_right = torch.tensor(LearnModelPhases.generator_unitary_matrix(), dtype=torch.complex128,
                                       device="cpu")

        self.block_left = block_left
        self.block_right = block_right

    def get_frobenius(self, phases, u):
        u_predict = self.get_u_device_real_bond(phases)
        n = u_predict.size()[0]
        return torch.sum(torch.abs(u_predict - u) ** 2) / n

    def func_loss(self, mini_batch):
        loss_list = torch.zeros(len(mini_batch), dtype=torch.float64, device='cpu')
        for k in range(len(mini_batch)):
            phases = mini_batch[k][0]
            u = mini_batch[k][1]
            loss_list[k] = self.get_frobenius(phases, u)
        loss = torch.mean(loss_list)
        return loss

    def optimize_adam(self, mini_batch_size, num_of_iters, verbose=True):
        self.block_left.requires_grad_(True)
        self.block_right.requires_grad_(True)

        optimizer = optim.Adam([self.block_left, self.block_right])

        mini_batch = self.data.get_mini_batch_train(mini_batch_size)

        func_loss_train = [self.func_loss(mini_batch).item()]
        func_loss_test = [self.func_loss(self.data.test_data).item()]

        if verbose:
            print('Iteration: ', 'start', ' Func loss of train mini_batch: ', func_loss_train[-1],
                  ', Func loss of test data: ', func_loss_test[-1])

        for iterations in range(num_of_iters):
            optimizer.zero_grad()
            mini_batch = self.data.get_mini_batch_train(mini_batch_size)

            f = self.func_loss(mini_batch)
            f.backward()

            optimizer.step()

            func_loss_train.append(self.func_loss(mini_batch).item())
            func_loss_test.append(self.func_loss(self.data.test_data).item())

            if verbose:
                print('Iteration: ', iterations, ' Func loss of train mini_batch: ', func_loss_train[-1],
                      ', Func loss of test data: ', func_loss_test[-1])

        self.block_left.requires_grad_(False)
        self.block_right.requires_grad_(False)

        return np.array(func_loss_train), np.array(func_loss_test)

    @staticmethod
    def get_phase_layer(phases):
        u = torch.zeros((4, 4), dtype=torch.complex128, device="cpu")
        u[0, 0] = torch.exp(1j * phases[0])
        u[1, 1] = torch.exp(1j * phases[1])
        u[2, 2] = torch.exp(1j * phases[2])
        u[3, 3] = torch.exp(1j * phases[3])
        return u

    @staticmethod
    def get_phase_matrix(elements):
        u = torch.diag(elements / torch.abs(elements))
        return u

    def get_u_device(self, phases):
        phase_layer = LearnModelPhases.get_phase_layer(phases)
        u = self.block_right @ phase_layer @ self.block_left
        return u

    def get_u_device_real_bond(self, phases):
        u = self.get_u_device(phases)

        phase_left = torch.conj(u[:, 0])
        phase_left_aux = torch.tensor([u[0, 0]] * len(phases), dtype=torch.complex128, device="cpu")
        phase_right = torch.conj(u[0, :])

        u = LearnModelPhases.get_phase_matrix(phase_left) @ LearnModelPhases.get_phase_matrix(phase_left_aux) @ u @ \
            LearnModelPhases.get_phase_matrix(phase_right)
        return u

    @staticmethod
    def fidelity(u, v):
        return (torch.trace(u @ v.T.conj()) * torch.trace(v @ u.T.conj())) / \
               (torch.trace(u @ u.T.conj()) * torch.trace(v @ v.T.conj()))

    @staticmethod
    def generator_diagonal_matrix(r, n):
        ph = np.random.randn(n, n) + 1j * np.random.randn(n, n)
        for i in range(n):
            for j in range(n):
                ph[i][j] = 0.0
        for i in range(n):
            ph[i][i] = r[i][i] / abs(r[i][i])
        return ph

    @staticmethod
    def generator_unitary_matrix(n=4):
        z = (np.random.randn(n, n) + 1j * np.random.randn(n, n)) / np.sqrt(2.0)
        q, r = np.linalg.qr(z)
        ph = LearnModelPhases.generator_diagonal_matrix(r, n)
        _u = np.dot(q, ph)
        return _u
