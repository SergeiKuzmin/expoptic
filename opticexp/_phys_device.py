import torch
import numpy as np


class PhysDevice(object):
    def __init__(self):
        self.phases_block_left = None
        self.phases_block_right = None
        self.phi_0 = None
        self.alpha = None

    def set_fix_params(self, phi_0, alpha, phases_block_left, phases_block_right):
        self.phases_block_left = phases_block_left
        self.phases_block_right = phases_block_right
        self.phi_0 = phi_0
        self.alpha = alpha

    @staticmethod
    def get_phase_matrix_2(phase):
        return torch.tensor([[torch.exp(1j * phase), 0.0],
                             [0.0, 1.0]], dtype=torch.complex128, device="cpu")

    @staticmethod
    def get_beam_splitter():
        return (1.0 / np.sqrt(2.0)) * torch.tensor([[1.0, 1.0],
                                                    [1.0, -1.0]], dtype=torch.complex128, device="cpu")

    @staticmethod
    def get_mach_zender(phase1, phase2):
        return PhysDevice.get_phase_matrix_2(phase2) @ PhysDevice.get_beam_splitter() @ \
               PhysDevice.get_phase_matrix_2(phase1) @ PhysDevice.get_beam_splitter()

    @staticmethod
    def get_mach_zender_truncate(phase):
        return PhysDevice.get_beam_splitter() @ PhysDevice.get_phase_matrix_2(phase) @ PhysDevice.get_beam_splitter()

    @staticmethod
    def get_block_mach_zender(phase1, phase2, idx_mode):
        u = torch.eye(4, dtype=torch.complex128, device="cpu")
        mach_zender = PhysDevice.get_mach_zender(phase1, phase2)
        u[idx_mode, idx_mode] = mach_zender[0, 0]
        u[idx_mode, idx_mode + 1] = mach_zender[0, 1]
        u[idx_mode + 1, idx_mode] = mach_zender[1, 0]
        u[idx_mode + 1, idx_mode + 1] = mach_zender[1, 1]
        return u

    @staticmethod
    def get_block_mach_zender_truncate(phase, idx_mode):
        u = torch.eye(4, dtype=torch.complex128, device="cpu")
        mach_zender = PhysDevice.get_mach_zender_truncate(phase)
        u[idx_mode, idx_mode] = mach_zender[0, 0]
        u[idx_mode, idx_mode + 1] = mach_zender[0, 1]
        u[idx_mode + 1, idx_mode] = mach_zender[1, 0]
        u[idx_mode + 1, idx_mode + 1] = mach_zender[1, 1]
        return u

    @staticmethod
    def get_block(phases):
        u = PhysDevice.get_block_mach_zender_truncate(phases[4], 2) @ \
            PhysDevice.get_block_mach_zender_truncate(phases[7], 1) @ \
            PhysDevice.get_block_mach_zender_truncate(phases[8], 0) @ \
            PhysDevice.get_block_mach_zender(phases[2], phases[3], 2) @ \
            PhysDevice.get_block_mach_zender(phases[5], phases[6], 1) @ \
            PhysDevice.get_block_mach_zender(phases[0], phases[1], 2)
        return u

    @staticmethod
    def get_phase_layer(phases):
        u = torch.eye(4, 4, dtype=torch.complex128, device="cpu")
        u[0, 0] = torch.exp(1j * phases[0])
        u[1, 1] = torch.exp(1j * phases[1])
        u[2, 2] = torch.exp(1j * phases[2])
        return u

    @staticmethod
    def get_phase_matrix(elements):
        u = torch.diag(elements / torch.abs(elements))
        return u

    def get_u_device(self, currents):
        phases = self.phi_0 + self.alpha @ (currents * currents)
        phase_layer = PhysDevice.get_phase_layer(phases)
        u = PhysDevice.get_block(self.phases_block_right) @ phase_layer @ PhysDevice.get_block(self.phases_block_left)
        return u

    def get_u_device_phases(self, phases):
        phase_layer = PhysDevice.get_phase_layer(phases)
        u = PhysDevice.get_block(self.phases_block_right) @ phase_layer @ PhysDevice.get_block(self.phases_block_left)
        return u

    def get_u_device_real_bond(self, currents):
        u = self.get_u_device(currents)
        phase_left = torch.conj(u[:, 0])
        u = PhysDevice.get_phase_matrix(phase_left) @ u
        phase_right = torch.conj(u[0, :])
        u = u @ PhysDevice.get_phase_matrix(phase_right)
        return u

    def get_u_device_real_bond_phases(self, phases):
        u = self.get_u_device_phases(phases)
        phase_left = torch.conj(u[:, 0])
        u = PhysDevice.get_phase_matrix(phase_left) @ u
        phase_right = torch.conj(u[0, :])
        u = u @ PhysDevice.get_phase_matrix(phase_right)
        return u
