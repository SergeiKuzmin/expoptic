import os
import torch
import numpy as np
from scipy import linalg
from random import shuffle
import matplotlib.pyplot as plt
import csv

from opticexp import Data, LearnModelCurrents


def get_phase_matrix(elements):
    _u = torch.diag(elements / torch.abs(elements))
    return _u


directory = './Data/'
list_of_files = []
for filename in os.listdir(directory):
    f = os.path.join(directory, filename)
    if os.path.isfile(f):
        list_of_files.append(f)

# print(list_of_files)

list_of_files_clear = []
for filename in list_of_files:
    if '.txt' in filename:
        list_of_files_clear.append(filename)

# print(list_of_files_clear)
# print(len(list_of_files_clear))

data_list = []

for filename in list_of_files_clear:
    file = open(filename, 'r')

    fidelity = float(filename.replace(directory, '').replace('_U_', '_').replace('.txt', '').split('_')[0]) / 1000.0

    for _ in range(3):
        file.readline()

    currents = file.readline()
    currents = currents.replace('\n', '')
    currents = currents.replace('  ', ' ')
    currents = currents.replace('  ', ' ')
    currents = list(map(lambda x: float(x), currents[1:-1].split(' ')))
    currents = torch.tensor(currents[1:], dtype=torch.float64, device='cpu') / 1000.0

    file.readline()

    phases = file.readline()
    phases = phases.replace('\n', '')
    phases = phases.replace('  ', ' ')
    phases = phases.replace('  ', ' ')
    phases = list(map(lambda x: float(x), phases[1:-1].split(' ')))
    phases = torch.tensor(phases, dtype=torch.float64, device='cpu')

    for _ in range(2):
        file.readline()

    real_matrix = []
    for i in range(4):
        row = file.readline()
        row = row.replace('\n', '')
        row = row.replace('  ', ' ')
        row = row.replace('  ', ' ')
        row_floats = list(map(lambda x: float(x), row[1:-1].split(' ')))
        real_matrix.append(row_floats)
    real_matrix = torch.tensor(real_matrix, dtype=torch.complex128, device='cpu')

    file.readline()

    imag_matrix = []
    for i in range(4):
        row = file.readline()
        row = row.replace('\n', '')
        row = row.replace('  ', ' ')
        row = row.replace('  ', ' ')
        row_floats = list(map(lambda x: float(x), row[1:-1].split(' ')))
        imag_matrix.append(row_floats)
    imag_matrix = torch.tensor(imag_matrix, dtype=torch.complex128, device='cpu')

    experiment_matrix = real_matrix + 1j * imag_matrix

    v = experiment_matrix

    phase_left = torch.conj(v[:, 0])
    phase_left_aux = torch.tensor([v[0, 0]] * 4, dtype=torch.complex128, device="cpu")
    phase_right = torch.conj(v[0, :])
    v = get_phase_matrix(phase_left) @ get_phase_matrix(phase_left_aux) @ v @ get_phase_matrix(phase_right)

    u, _ = linalg.polar(experiment_matrix)
    u = torch.tensor(u, dtype=torch.complex128, device='cpu')

    phase_left = torch.conj(u[:, 0])
    phase_left_aux = torch.tensor([u[0, 0]] * 4, dtype=torch.complex128, device="cpu")
    phase_right = torch.conj(u[0, :])
    u = get_phase_matrix(phase_left) @ get_phase_matrix(phase_left_aux) @ u @ get_phase_matrix(phase_right)

    data_list.append({'Fidelity': fidelity,
                      'Currents': currents,
                      'Phases': phases,
                      'ExperimentMatrix': experiment_matrix,
                      'ExperimentMatrixRealBoard': v,
                      'ExperimentUnitaryMatrixRealBoard': u})

    file.close()

# print(len(data_list))
#
# print(data_list[0])
#

n = len(data_list)
print(n)

train_test_data = []
for k in range(0, n, 1):
    train_test_data.append((data_list[k]['Currents'], data_list[k]['ExperimentUnitaryMatrixRealBoard'],
                            data_list[k]['Fidelity']))

# print(len(train_test_data))

shuffle(train_test_data)

# print(len(train_test_data))

# print(train_test_data)

m_train = 60

data_train = train_test_data[0:m_train]
data_test = train_test_data[m_train:]

print(len(data_train), len(data_test))

data = Data()
data.set_train_data(data_train)
data.set_test_data(data_test[0:1])

learn_model = LearnModelCurrents(data)

# alpha_answer = torch.tensor([[5.977, 2.446, 1.367],
#                              [-0.414, 8.39, 3.789],
#                              [-2.782, 2.104, 9.439]], dtype=torch.float64, device="cpu")
# #
# phi_0_answer = torch.tensor([0.895, 0.957, 0.212], dtype=torch.float64, device="cpu")

# 6.88 1.71 −1.71
# 2.71 5.88 0
# 1 1.71 4.17

alpha_answer = torch.tensor([[4.17, 1.71, 1.0],
                             [0.0, 5.88, 2.71],
                             [-1.71, 1.71, 6.88]], dtype=torch.float64, device="cpu")

phi_0_answer = torch.tensor([0.0, 0.0, 0.0], dtype=torch.float64, device="cpu")

learn_model.set_start_parameters(alpha=alpha_answer, phi_0=phi_0_answer)

learn_model.optimize_adam(5, 6000, verbose=True)

# data_train = list(filter(lambda x: learn_model.fidelity(learn_model.get_u_device_real_bond(x[0]), x[1]).real >= 0.95,
#                          data_train))
#
# data.set_train_data(data_train)
#
# learn_model.optimize_adam(5, 1000, verbose=True)

fidelity_list = []
for i in range(len(data_test)):
    fidelity_list.append(learn_model.fidelity(learn_model.get_u_device_real_bond(data_test[i][0]),
                                              data_test[i][1]).real)

print(len(fidelity_list))

fig, ax = plt.subplots()

# Рассчитываем гистограмму
counts, bins = np.histogram(fidelity_list, bins=100)

# Нормализуем гистограмму, чтобы получить вероятности
counts_normalized = counts / counts.sum()

# Строим нормализованную гистограмму с чёрной обводкой каждого бина
plt.bar(bins[:-1], counts_normalized, width=np.diff(bins), color='navy', alpha=0.5, edgecolor='black', align='edge')

# Добавляем сетку со сплошной линией
plt.grid(True, linestyle='-', linewidth=0.5, color='gray', alpha=0.5)

plt.tick_params(which='major', labelsize=18)
plt.tick_params(which='minor', labelsize=18)

# ax.legend(loc='upper right')
plt.xlabel('Fidelity with predicted by model', fontsize=20)
plt.ylabel('Probability', fontsize=20)
plt.show()

# Открываем файл для записи
with open('./Results/currents.csv', 'w', newline='') as file:
    writer = csv.writer(file)

    # Записываем заголовки столбцов
    writer.writerow(['X', 'Y'])

    # Записываем данные из списка кортежей
    for x in data_test:
        writer.writerow([learn_model.fidelity(learn_model.get_u_device_real_bond(x[0]), x[1]).real.item(), x[2]])

# Файл сохранен и закрыт
print("Данные успешно сохранены в файл 'output.csv'.")
